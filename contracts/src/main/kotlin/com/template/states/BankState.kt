package com.template.states

import com.template.contracts.BankContract
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.AnonymousParty
import net.corda.core.identity.Party
import java.security.PublicKey

// *********
// * State *
// *********
@BelongsToContract(BankContract::class)
data class BankState(val Value: Int,
                val Sender: PublicKey,
                val Reciever: PublicKey,
                override val linearId: UniqueIdentifier = UniqueIdentifier())
    : LinearState {
    override val participants: List<AbstractParty> get() = listOf(Sender, Reciever).map { AnonymousParty(it)}
}