package com.template.flows

import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import co.paralleluniverse.fibers.Suspendable
import com.template.contracts.BankContract
import com.template.states.BankState
import net.corda.core.flows.*
import net.corda.core.utilities.ProgressTracker
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import com.r3.corda.lib.accounts.workflows.accountService
import com.r3.corda.lib.accounts.workflows.flows.RequestKeyForAccount
import net.corda.core.identity.Party


import java.util.*

// *********
// * Flows *
// *********
@InitiatingFlow
@StartableByRPC
class TransferFlow(val TransferValue: Int, val Sender: UUID, val Reciever: UUID) : FlowLogic<SignedTransaction>() {
    override val progressTracker = ProgressTracker()

    /** The flow logic is encapsulated within the call() method. */
    @Suspendable
    override fun call(): SignedTransaction {
        // We retrieve the notary identity from the network map.
        val notary = serviceHub.networkMapCache.notaryIdentities[0]

        val senderAccountInfo = accountService.accountInfo(Sender) ?: throw IllegalStateException("Can't find account to move from $Sender")
        val reciverAccountInfo = accountService.accountInfo(Reciever) ?: throw IllegalStateException("Can't find account to move to $Reciever")

        val senderKey = subFlow(RequestKeyForAccount(senderAccountInfo.state.data)).owningKey
        val reciverKey = subFlow(RequestKeyForAccount(reciverAccountInfo.state.data)).owningKey

        // We create the transaction components.
        val outputState = BankState(TransferValue, senderKey, reciverKey)
        val command = Command(BankContract.Create(), listOf(senderKey, reciverKey))

        // We create a transaction builder and add the components.
        val txBuilder = TransactionBuilder(notary = notary)
                .addOutputState(outputState, BankContract.ID)
                .addCommand(command)

        // Verifying the transaction.
        txBuilder.verify(serviceHub)

        var keysToSignWith = mutableListOf(ourIdentity.owningKey, senderKey)
        //Only add the borrower account if it is hosted on this node (potentially it might be on a different node)
        if (reciverAccountInfo.state.data.host == serviceHub.myInfo.legalIdentitiesAndCerts.first().party) {
            keysToSignWith.add(reciverKey)
        }
        val locallySignedTx = serviceHub.signInitialTransaction(txBuilder, keysToSignWith)

        //We have to do 2 different flows depending on whether the other account is on our node or a different node
        if (reciverAccountInfo.state.data.host == serviceHub.myInfo.legalIdentitiesAndCerts.first().party) {
            //Notarise and record the transaction in just our vault.

            return subFlow(FinalityFlow(locallySignedTx))
        } else {

            val borrowerSession = initiateFlow(reciverAccountInfo.state.data.host)
            val borrowerSignature = subFlow(CollectSignatureFlow(locallySignedTx, borrowerSession, reciverKey))
            val fullySignedTx = locallySignedTx.withAdditionalSignatures(borrowerSignature)


            return subFlow(FinalityFlow(fullySignedTx, listOf(borrowerSession)))
        }
    }
}


@InitiatedBy(TransferFlow::class)
class FlowResponder(val counterpartySession: FlowSession) : FlowLogic<Unit>() {
    @Suspendable
    override fun call() : Unit {
        val signTransactionFlow = object : SignTransactionFlow(counterpartySession) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                "No inputs should be consumed" using (stx.tx.inputs.isEmpty())
                val output = stx.tx.outputs.single().data
                "This must be a valid amount transaction." using (output is BankState)
            }
        }
        val expectedTxId = subFlow(signTransactionFlow).id
        subFlow(ReceiveFinalityFlow(counterpartySession, expectedTxId))
    }
}

@StartableByRPC
@InitiatingFlow
class ShareAccountTo(
        private val acctNameShared: String,
        private val shareTo: Party
) : FlowLogic<String>(){

    @Suspendable
    override fun call(): String {

        //Create a new account
        val AllmyAccounts = accountService.ourAccounts()
        val SharedAccount = AllmyAccounts.single { it.state.data.name == acctNameShared }.state.data.identifier.id
        accountService.shareAccountInfoWithParty(SharedAccount,shareTo)

        return "Shared " + acctNameShared + " with " + shareTo.name.organisation
    }
}